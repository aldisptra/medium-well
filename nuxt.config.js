import colors from "vuetify/es5/util/colors";

export default {
  ssr: false,
  target: "static",
  head: {
    titleTemplate: "",
    title: "medium well",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },
  axios: {
    baseURL: process.env.API_GATEWAY
      ? process.env.API_GATEWAY
      : 'https://supergateway.jhc.asia/api',
  },
  auth: {
    redirect: {
      login: false,
      logout: false,
      callback: false,
      home: '/app/home',
    },
    strategies: {
      local: {
        token: {
          property: 'token',
          type: 'Bearer',
          global: true,
          maxAge: 86400,
        },
        user: {
          property: 'results',
        },
        endpoints: {
          login: { url: '/api/login', method: 'post' },
          logout: { url: '/api/logout', method: 'post' },
          user: { url: '/api/me', method: 'get' },
        },
      },
    },
  },
  plugins: [],

  router: {
    middleware: "auth",
  },

  components: true,

  buildModules: ["@nuxtjs/vuetify"],

  modules: ["@nuxtjs/axios", "@nuxtjs/auth-next", "@nuxtjs/toast"],

  vuetify: {
    font: {
      family: "Montserrat",
    },
    customVariables: ["~/assets/variables.scss"],
    theme: {
      light: true,
      themes: {
        light: {
          primary: "#16347A",
          accent: "#526CFF",
          secondary: "#F2994A",
          info: "#06A0C1",
          warning: "#F2994A",
          error: "#EB5757",
          success: "#7BBC78",
          basic: "#FAFAFA",
          nonbasic: "#000000",
        },
      },
    },
  },
  build: {},
};
